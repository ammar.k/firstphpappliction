<?php
    session_start();
    ob_start();

?>
<body>

    <?php 

    if(!isset($_SESSION['products']) || empty($_SESSION['products'])){
            echo "<p>Aucun produit en saisin ...</p>";
        }

        else {
            
            echo "<table>",
                    "<thead>",
                    "<tr>",
                        "<th>#</th>",
                        "<th>Produit</th>",
                        "<th>Description</th>",
                        "<th>Prix</th>",
                        "<th>Quantité</th>",
                        "<th>Total</th>",
                    "</tr>",
                    "</thead>",
                "</tbody>";
                $totalGeneral = 0;
                $nbProduit = 0;
                foreach($_SESSION['products'] as $index => $product){
                    $btnUP = '<a href="traitement.php?action=qttup&id=' . $index . '"> + </a>';
                    $btnDOWN = '<a href="traitement.php?action=qttdown&id=' . $index . '"> - </a>';
                    //add icon to the button
                    $btdelete = '<a href="traitement.php?action=delete&id=' . $index . '">x</a>';
                $result = " 
                 <tr>
                    <td>" . $index. "</td>
                    <td>" . $product['name'] . "</td>
                    <td style='color:blue';>" . $product['description'] . "</td>
                    <td>" . number_format($product['price'], 2 ,",","&nbsp;") . "&nbsp €</td>
                    <td>" . $product['qtt'] . $btnDOWN . $btnUP . "</td>
                    <td>" . number_format($product['total'], 2 ,",","&nbsp;") . "&nbsp € $btdelete </td>
                </tr>";
                echo $result;
                $totalGeneral += $product['total'];
                $nbProduit += $product['qtt'];
            }

            echo "<tr>",
                    "<td colspan='4'>Nombre de Produit</td>",
                    "<td>" . $nbProduit . "</td>",
                "</tr>",
                "</tbody>";
                
            

            echo "<tr>",
                    "<td colspan='4'>Total général</td>",
                    "<td>" . number_format($totalGeneral, 2 ,",","&nbsp;") . "&nbsp €</td>",
                "</tr>",
                "</tbody>",
                "</table>";

        }

        if(isset($_SESSION['message'])){

            echo "<p>" . $_SESSION['message'] . "</p>";
            unset($_SESSION['message']);
        }

        
    
    ?>
    
        <form action="traitement.php?action=deleteall" method="post">
            <p>
                <label>Clear All Products :
                </label>
            </p>
            <p>
             
            <p>
                <input type="submit" name="submit" value="Delete All">
    </p>


        </form>
</body>
</body>
</html>

<?php
    $content = ob_get_clean();
    $title = "Panier";
    $header = "Récapitulatif des produits";
    require_once "template.php";

?>