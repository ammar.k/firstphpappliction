<?php

    session_start();

    if(isset($_GET['action'])){

        switch ($_GET['action']) {
            case 'add':
                 //filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING
                $name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING);
                $price = filter_input(INPUT_POST, "price", FILTER_VALIDATE_FLOAT,FILTER_FLAG_ALLOW_FRACTION);
                $qtt = filter_input(INPUT_POST, "qtt", FILTER_VALIDATE_INT);
                $description = filter_input(INPUT_POST, "description", FILTER_SANITIZE_STRING);
                
                if($name && $price && $qtt){

                $product = [
                    "name" => $name,
                    "description" => $description,
                    "price" => $price,
                    "qtt" => $qtt,
                    "total" => $price * $qtt
                ];
                $_SESSION['products'][] = $product;
                $_SESSION['message'] = "product has been added successfully";

             }
                header("location: index.php");
                break;

            case 'delete':
                $index = $_GET['id'];
                if($index !== false && isset($_SESSION['products'][$index])){
                unset($_SESSION['products'][$index]);
                $_SESSION['message'] = "product has been deleted successfully";
                $_SESSION['products'] = array_values($_SESSION['products']);
            }

            else{
                $lastIndex = count($_SESSION['products']) - 1;
                $_SESSION['message'] = "the product does not exist, enter a number between
                0 and " . $lastIndex;
                
            }
                header("location: recap.php");
                break;
            
            case 'qttup':
                $index = $_GET['id'];
                if(isset($_SESSION['products'][$index])){
                $_SESSION['products'][$index]['qtt']++;
                $_SESSION['products'][$index]['total'] = $_SESSION['products'][$index]['qtt'] *
                $_SESSION['products'][$index]['price'];
            }
        
                header("location: recap.php");
                break;

            case 'qttdown':
                $index = $_GET['id'];
                if(isset($_SESSION['products'][$index])){
                    if($_SESSION['products'][$index]['qtt'] == 1){
                        unset($_SESSION['products'][$index]);
                        $_SESSION['products'] = array_values($_SESSION['products']);

                    }
                    else{
                        $_SESSION['products'][$index]['qtt']--;
                        $_SESSION['products'][$index]['total'] = $_SESSION['products']
                        [$index]['qtt'] * $_SESSION['products'][$index]['price'];

                    }
            }
                header("location: recap.php");
                break;

            case 'deleteall':    

                unset($_SESSION['products']);
                $_SESSION['message'] = "All products have been deleted successfully";
                header("location: recap.php");
                break;

            
            
            default:
                # code...
                break;
        }

    }

?>