<?php
session_start();
ob_start();
?>


<body>
    <dev id="container">
    <form action="traitement.php?action=add" method="post">
        <p>
            <labelNom> Nom De Produit :
            <input type="text" name="name">
            </label>
        </p>
        <p>
            <labelNom> Prix De Produit :
            <input type="number" step="any" min="1"  name="price">
            </label>
        </p>
        <p>
            <labelNom> Quantité Désirée :
            <input type="number" name="qtt" min="1" value="1">
            </label>
        </p>
        <p>
            <labelNom> Desciprtion De Produit :
            <textarea name="description" cols="90" rows="10" placeholder="Décrire le produit"></textarea>
            </label>
        </p>
        <p>
            <input type="submit" name="submit" value="Ajouter le produit">
        </p>
        

    </form>
    </dev>

    <?php
    if(isset($_SESSION['message'])){

        echo "<p>" . $_SESSION['message'] . "</p>";
        unset($_SESSION['message']);
    }
    ?>
</body>
</html>

<?php
$content = ob_get_clean();
$title = "Ajouter un produit";
$header = "Ajouter un produit";
require_once "template.php";
?>